$(document).ready(function () {
    let $btns = $('.site-main .portfolio-area .button-group button');
    $('.site-main .portfolio-area .grid').isotope({

        filter: '*',
    });
    $btns.click((function (e) {
        $('.site-main .portfolio-area .button-group button').removeClass('active');
        e.target.classList.add('active');
        let selector = $(e.target).attr('data-filter');

        $('.site-main .portfolio-area .grid').isotope({
            filter: selector,

        })
        return false;
    }))

    $(".site-main .carousel_references").slick({
        infinite: true,
        centerPadding: '20%',
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
    });

    let $btnss = $('.site-main .nav-bar-area #navbarNav .button-group button');

    $btnss.click((function (e) {
        $('.site-main .nav-bar-area #navbarNav .button-group button').removeClass('active');
        e.target.classList.add('active');
    }))
    new AOS.init({ disable: 'mobile' });
})
const handleScrollto = (content) => {
    let elmnt = document.getElementById(content);
    elmnt.scrollIntoView();
}
const handleClickGotoPage = (url) => {
    window.open(url, "_self");
}