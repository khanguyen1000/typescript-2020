$(document).ready(function () {
    $(".portfolio-detail-area .slick__carousel").slick({
        infinite: true,
        centerPadding: '20%',
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
    });
    $(".portfolio-detail-area .slick__project").slick({
        dots: false,
        vertical: true,
        infinite: true,
        verticalSwiping: true,
        arrows: false,
        swipeToSlide: true,
        autoplaySpeed: 1000,
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        speed: 500,
    });
    $(".portfolio-detail-area .slick__projectH").slick({
        infinite: true,
        centerPadding: '5px',
        dots: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        autoplaySpeed: 1000,
        autoplay: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 749,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows: false,
                }
            }

        ]

    });
    new AOS.init({ disable: 'mobile' });
})
const handleScrollto = (content) => {
    let elmnt = document.getElementById(content);
    elmnt.scrollIntoView();
}
const handleGoto = () => {
    window.history.back();
}