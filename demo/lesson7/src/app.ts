// class
class Invoive {
  client: string;
  details: string;
  amount: number;
  constructor(c: string, d: string, a: number) {
    this.client = c;
    this.details = d;
    this.amount = a;
  }
  format() {
    return `${this.client} owes $${this.amount} for ${this.details}  `;
  }
}

const invOne = new Invoive("mario", "work on mario website", 250);
console.log(invOne.format());
const invTwo = new Invoive("mmao", "work on mamao website", 350);

let invoices: Invoive[] = [];
invoices.push(invOne);
invoices.push(invTwo);

// console.log(invoices);

// class employee{
//   public name:string;
//   readonly age:number;
//   private timeWork:number;
//   constructor(n:string,a:number,tw:number){
//     this.name=n;
//     this.age=a;
//     this.timeWork=tw;
//   }
//   calcSalary:Function=():number=>{
//     return this.timeWork*1000;
//   }
// }

class staff {
  constructor(public name: string) {}
  calcSalary: Function = (): number => {
    return 1000;
  };
}
class manager extends staff {
  constructor(name: string) {
    super(name);
  }
  calcSalary: Function = (): number => {
    return super.calcSalary() + 2000;
  };
}
class Animal {
  constructor(public name: string, public type = "Animal") {}
  sayType: Function = (): void => {
    console.log("type: " + this.type);
  };
  sayName: Function = (): void => {
    console.log("name: " + this.name);
  };
}
class Dog extends Animal {
  constructor(name: string) {
    super(name);
    this.type = "Dog";
  }
  shout: Function = (): void => {
    console.log("shout: " + "Gau Gau Gau !!!");
  };
}
