"use strict";
// class
class Invoive {
    constructor(c, d, a) {
        this.client = c;
        this.details = d;
        this.amount = a;
    }
    format() {
        return `${this.client} owes $${this.amount} for ${this.details}  `;
    }
}
const invOne = new Invoive("mario", "work on mario website", 250);
console.log(invOne.format());
const invTwo = new Invoive("mmao", "work on mamao website", 350);
let invoices = [];
invoices.push(invOne);
invoices.push(invTwo);
// console.log(invoices);
