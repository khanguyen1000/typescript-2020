// Enums

enum ResourceType {
  BOOK,
  AUTHOR,
  FILM,
  DIRECTOR,
  PERSON,
}

interface Resource<T> {
  uid: number;
  resourceName: ResourceType;
  data: T;
}

const docOne: Resource<string> = {
  uid: 1,
  resourceName: ResourceType.AUTHOR,
  data: "shaul",
};
const docTwo: Resource<string[]> = {
  uid: 1,
  resourceName: ResourceType.BOOK,
  data: ["John", "Peter"],
};
console.log(docOne, docTwo);
