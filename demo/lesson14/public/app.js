"use strict";
// Enums
var ResourceType;
(function (ResourceType) {
    ResourceType[ResourceType["BOOK"] = 0] = "BOOK";
    ResourceType[ResourceType["AUTHOR"] = 1] = "AUTHOR";
    ResourceType[ResourceType["FILM"] = 2] = "FILM";
    ResourceType[ResourceType["DIRECTOR"] = 3] = "DIRECTOR";
    ResourceType[ResourceType["PERSON"] = 4] = "PERSON";
})(ResourceType || (ResourceType = {}));
const docOne = {
    uid: 1,
    resourceName: ResourceType.AUTHOR,
    data: "shaul",
};
const docTwo = {
    uid: 1,
    resourceName: ResourceType.BOOK,
    data: ["John", "Peter"],
};
console.log(docOne, docTwo);
