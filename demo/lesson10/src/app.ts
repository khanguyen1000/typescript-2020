// interface
interface IsPerson {
  name: string;
  age: number;
  speak(a: string): void;
  spend(a: number): number;
}
const me: IsPerson = {
  name: "kha",
  age: 21,
  speak(text: string): void {
    console.log(text);
  },
  spend(amount: number): number {
    console.log("i spend ", amount);
    return amount;
  },
  // skill:[],//error
};
// console.log(me, me.speak("hi everyone"));

const greetPerson = (person: IsPerson) => {
  console.log(`hello ${person.name}`);
};
greetPerson(me);
