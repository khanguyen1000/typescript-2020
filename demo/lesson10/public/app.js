"use strict";
const me = {
    name: "kha",
    age: 21,
    speak(text) {
        console.log(text);
    },
    spend(amount) {
        console.log("i spend ", amount);
        return amount;
    },
};
// console.log(me, me.speak("hi everyone"));
const greetPerson = (person) => {
    console.log(`hello ${person.name}`);
};
greetPerson(me);
