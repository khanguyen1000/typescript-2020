// class
// class Invoive {
//   // readonly client: string;
//   // private details: string;
//   // public amount: number;

//   constructor(
//     readonly client: string,
//     private details: string,
//     public amount: number
//   ) {}

//   format() {
//     return `${this.client} owes $${this.amount} for ${this.details}  `;
//   }
// }
import Invoive from "./classes/invoice.js";

const invOne = new Invoive("mario", "work on mario website", 250);
console.log(invOne.format());
const invTwo = new Invoive("mmao", "work on mamao website", 350);

let invoices: Invoive[] = [];
invoices.push(invOne);
invoices.push(invTwo);

invoices.forEach((inv) => {
  // console.log(inv.detail); //error attribute is private
  // invOne.client='jacket'; //error just readonly
  console.log(inv.client, inv.amount, inv.format()); //another public
});
// console.log(invoices);

import { staff } from "./classes/staff.js";

let staffOne = new staff("Kha");

staffOne.calcSalary();
