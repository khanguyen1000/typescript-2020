// // this old function we used it in javascript .
// let sayhello = () => {
//   console.log("helow world");
// };

// Typescript
// let sayHello: Function;
// // sayHello ='hello '; //error
// sayHello = () => {
//   console.log("hello again");
// };

// use symbol  ' ? ' if u want to add params
// const add = (a: number, b: number, c?: number | string) => {
//   console.log(a + b);
//   console.log(c);
// };
// add(5, 10); //ok
// add(5, 10, "kha"); //ok

// use default params
// const DefaultParams = (a: number, b: number, c: number | string = "kha") => {
//   console.log(a + b);
//   console.log(c);
// };

let SpreadOperator: Function = (...arr: Array<string>) => {
  console.log(arr);
};
SpreadOperator("one"); //["one"]
SpreadOperator("one", "two"); //["one","two"]
SpreadOperator("one", "two", "three"); //["one","two","three"]

const DefaultParams = (
  a: number,
  b: number,
  c: number | string = "kha"
): void => {
  console.log(a + b);
  console.log(c);
};
DefaultParams(5, 10); //ok
DefaultParams(5, 10, "Mais"); //ok

const noReturnValue: Function = () => {
  console.log("Cybersoft");
};

// return values
// const minus = (a:number,b:number)=>{
//     return a -b;
// }
const minus = (a: number, b: number): number => {
  return a - b;
};

let result = minus(10, 5);
