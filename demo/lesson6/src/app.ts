// // dom tag
// c1 dom vs kieu ngau nhien
// const anchor = document.querySelector("a")!;
// console.log(anchor.href);

// const anchor = document.querySelector("a") as HTMLAnchorElement;
// console.log(anchor.href);

// form
// const form =document.querySelector('form')!;

// console.log(form.children);

// inputs
const type = document.querySelector("#type") as HTMLSelectElement;
const toFrom = document.querySelector("#toFrom") as HTMLInputElement;
const details = document.querySelector("#details") as HTMLInputElement;
// const amount = document.querySelector("#amount") as HTMLInputElement;
const form = document.querySelector(".new-item-form") as HTMLFormElement;
form.addEventListener("submit", (e: Event) => {
  e.preventDefault();
  //submit method
});

//tag
let tag = document.getElementsByTagName("a");
// let tag = document.querySelector("a");
// class
// let clas = document.getElementsByClassName("form-group");
let clas = document.querySelector(".form-group");
// id
// let uid = <HTMLInputElement>document.getElementById("name"); //cách 1
let uid = <HTMLInputElement>document.querySelector("#name"); //cách 1.1
// let uid = document.getElementById("name") as HTMLInputElement; //cách 2
// let uid = document.querySelector("#name") as HTMLInputElement; //cách 2.2

const amount = document.querySelector("#amount") as HTMLInputElement;
let value = amount.valueAsDate;
console.log(value);
