export class MarkEmpl {
    constructor(idIp, nameIp, ageIp, genderInp, timeWorkedInp, departmentImp) {
        this.id = idIp;
        this.name = nameIp;
        this.age = ageIp;
        this.gender = genderInp;
        this.timeWorked = timeWorkedInp;
        this.department = departmentImp;
    }
    calcSalary() {
        let sal = this.timeWorked * 45000 * 1.25;
        return sal;
    }
    render() {
        let templateOutput = "";
        templateOutput = `
    
    <tr>
      <td scope="row">1</td>
      <td>${this.name}</td>
      <td>${this.age}</td>
      <td>${this.gender}</td>
      <td>${this.department}</td>
      <td>${this.calcSalary().toLocaleString()}</td>
      <td>
          <button class="btn btn-primary" id="btnUpdate_${this.id}"><i class="fas fa-pencil-alt"></i></button>
          <button class="btn btn-primary"><i class="far fa-window-close"></i></button>
      </td>
    </tr>
    
    `;
        return templateOutput;
    }
}
