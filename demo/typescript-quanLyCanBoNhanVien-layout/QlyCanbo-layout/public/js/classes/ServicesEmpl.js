export class ServicesEmpl {
    constructor() { }
    getAllEmpl() {
        return axios({
            url: "http://localhost:3000/employees",
            method: "get",
        }).catch((err) => {
            console.log(err);
        });
    }
    addNewEmpl(empl) {
        return axios({
            url: "http://localhost:3000/employees",
            method: "post",
            data: empl,
        }).catch((err) => {
            console.log(err);
        });
    }
}
