export interface IRender {
  render(): string;
}
