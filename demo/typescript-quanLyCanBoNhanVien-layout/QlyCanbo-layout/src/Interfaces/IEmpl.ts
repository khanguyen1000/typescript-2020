export interface IEmpl {
  id: string;
  name: string;
  age: number;
  gender: string;
  timeWorked: number;
  department: string;
  calcSalary(): number;
}
