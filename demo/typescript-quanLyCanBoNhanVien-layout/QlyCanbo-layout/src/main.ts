// fetch("http://localhost:3000/employees")
//   .then((res) => res.json())
//   .then((data) => console.log(data))
//   .catch((err) => console.log(err));

// import Classes
import { ITEmpl } from "./classes/ITEmpl.js";
import { MarkEmpl } from "./classes/MarkEmpl.js";
import { SalesEmpl } from "./classes/SalesEmpl.js";
import { ServicesEmpl } from "./classes/ServicesEmpl.js";
import { IEmpl } from "./Interfaces/IEmpl.js";
import { IRender } from "./Interfaces/IRender.js";

// DOM table
let TbTable = document.querySelector("#tb_render") as HTMLTableDataCellElement;
// Khai bao axios
declare let axios: any;
declare let swal: any;
declare let $: any;
// Phân tích đối tượng cần có
type ListEmplType = (MarkEmpl | ITEmpl | SalesEmpl)[];
let listEmpls: ListEmplType = [];
let companyServices = new ServicesEmpl();
const MapData: Function = (data: ListEmplType) => {
  return data.map((item) => {
    let { id, name, age, gender, timeWorked, department } = item;
    let Empl: IEmpl & IRender;

    if (item.department.toLowerCase() === "it") {
      return (Empl = new ITEmpl(id, name, age, gender, timeWorked, department));
    } else if (item.department.toLowerCase() === "marketing") {
      return (Empl = new MarkEmpl(
        id,
        name,
        age,
        gender,
        timeWorked,
        department
      ));
    } else {
      return (Empl = new SalesEmpl(
        id,
        name,
        age,
        gender,
        timeWorked,
        department
      ));
    }
  });
};

const HandleCreateUpdateMethod: Function = (id: string): void => {
  let btnUpdate = document.querySelector(
    `btnUpdate_${id}`
  ) as HTMLButtonElement;
  btnUpdate.addEventListener("click", () => {
    $("#modalAddNew").modal({
      open: true,
    });
  });
};

const RenderTableMethod: Function = (): void => {
  // duyệt mảng để tạo ra 1 template chung
  let template = "";

  for (let item of listEmpls) {
    template += item.render();
  }
  // console.log(template);
  // DOM dến table và add vào
  TbTable.innerHTML = template;
};

// axios({
//   url: "http://localhost:3000/employees",
//   method: "get",
// })
//   .catch((err: any) => console.log(err))
//   .then((res: any) => {
//     console.log(res.data);
//     listEmpls = MapData(res.data);
//     console.log(listEmpls);
//     RenderTableMethod();
//   });

companyServices.getAllEmpl().then((res: any) => {
  // console.log(res.data);
  listEmpls = MapData(res.data);
  // console.log(listEmpls);
  RenderTableMethod();
});

//#region DOM event
// DOM inputs
let txtName = document.querySelector("#txtName") as HTMLInputElement;
let txtAge = document.querySelector("#txtAge") as HTMLInputElement;
let txtGender = document.querySelector("#txtGender") as HTMLInputElement;
let txtTimeWorked = document.querySelector(
  "#txtTimeWorked"
) as HTMLInputElement;
// DOM radio button
let radIT = document.querySelector("#radIT") as HTMLInputElement;
let radMark = document.querySelector("#radMark") as HTMLInputElement;
let radSal = document.querySelector("#radSal") as HTMLInputElement;

// DOM Form
let form = document.querySelector("#frm-add") as HTMLFormElement;

const handleSubmit: Function = () => (e: Event): void => {
  e.preventDefault();
  let name = txtName.value;
  let age = txtAge.valueAsNumber;
  let gender = txtGender.value;
  let timeWorked = txtTimeWorked.valueAsNumber;
  let department: string;
  if (radIT.checked) {
    department = radIT.value;
  } else if (radMark.checked) {
    department = radMark.value;
  } else {
    department = radSal.value;
  }
  const data = { name, age, gender, timeWorked, department };
  console.log(data);
  companyServices.addNewEmpl(data).then((res: any) => {
    swal({
      title: `Thêm thành công`,
      icon: "success",
    });
  });
};

form.addEventListener("submit", handleSubmit());
//#endregion
