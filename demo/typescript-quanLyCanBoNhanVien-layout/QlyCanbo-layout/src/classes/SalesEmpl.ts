import { IEmpl } from "../Interfaces/IEmpl";
import { IRender } from "../Interfaces/IRender";

export class SalesEmpl implements IEmpl, IRender {
  id: string;
  name: string;
  age: number;
  gender: string;
  timeWorked: number;
  department: string;
  constructor(
    idIp: string,
    nameIp: string,
    ageIp: number,
    genderInp: string,
    timeWorkedInp: number,
    departmentImp: string
  ) {
    this.id = idIp;
    this.name = nameIp;
    this.age = ageIp;
    this.gender = genderInp;
    this.timeWorked = timeWorkedInp;
    this.department = departmentImp;
  }
  calcSalary(): number {
    let sal = this.timeWorked * 48000 * 1.5;
    return sal;
  }
  render(): string {
    let templateOutput: string = "";
    templateOutput = `
    
    <tr>
      <td scope="row">1</td>
      <td>${this.name}</td>
      <td>${this.age}</td>
      <td>${this.gender}</td>
      <td>${this.department}</td>
      <td>${this.calcSalary().toLocaleString()}</td>
      <td>
          <button class="btn btn-primary" id="btnUpdate_${
            this.id
          }"><i class="fas fa-pencil-alt"></i></button>
          <button class="btn btn-primary"><i class="far fa-window-close"></i></button>
      </td>
    </tr>
    
    `;
    return templateOutput;
  }
}
