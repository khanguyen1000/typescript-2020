import { IEmpl } from "../Interfaces/IEmpl";
import { IRender } from "../Interfaces/IRender";

declare let axios: any;

export class ServicesEmpl {
  constructor() {}
  getAllEmpl(): any {
    return axios({
      url: "http://localhost:3000/employees",
      method: "get",
    }).catch((err: Error) => {
      console.log(err);
    });
  }
  addNewEmpl(empl: any): any {
    return axios({
      url: "http://localhost:3000/employees",
      method: "post",
      data: empl,
    }).catch((err: Error) => {
      console.log(err);
    });
  }
}
