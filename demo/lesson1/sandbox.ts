// ==================== type basic =============================

// **********string, number, boolean *********
let character = 'Nguyen Ngoc Kha';
let age = 30;
let isBlackBelt = false;

const circ = (diameter: number) => {
  return diameter * Math.PI;
};

console.log(circ(1));

// **********array*********
// let names = ["kha", "luigi", "mario"];
// // names='helloe';  error
// names.push("toad");
// names.push("2");

// let numbers = [10, 20, 30, 40];
// numbers.push(25);
// // numbers.push('a'); error

// let mixed = ["ken", 4, "5", 1, true];

// mixed.push("rye");
// mixed.push(1);
// mixed.push(true);

// **********objects*********
// let nv = {
//   name: "Nguyen Ngoc Kha",
//   age: 21,
// };

// nv.name = "kha";
// nv.age = 22;
// // nv.age='25';  error
// // nv.skills=['html','css','reactjs','angular'];  error ko tồn tại

// // lỗi nếu ko đúng
// nv = {
//     name: "goc Kha",
//     // age: 21,
// };

// // =================== Explicit Types =======================

// let character: string;
// let age: number;
// let isDev: boolean;

// // age='stirng'; error
// age = 30;

// // isDev=20; error
// isDev = true;

// // ********* array ************

// let skills: string[];

// skills.push("kha"); //error
// // sandbox.js:46 Uncaught TypeError: Cannot read property 'push' of undefined để fix khi ta cấp cho nó vùng nhớ ngây khi vừa khởi tạo

// // arr=[10,20]; error
// skills = ["Angular", "ReactJs", "html5", "scss", "css"];
// skills.push(15);

// // ********* union types ************

// let mixed: (string | number)[] = [];
// mixed.push("hi");
// mixed.push(1);
// // mixed.push(true); //error

// let uid: string | boolean;
// uid = "kha";
// uid = true;
// // uid=2; //error

// // ********* objects ************

// let userOne: object;
// userOne = { name: "abc", age: 30 };
// // userOne='string'; //error
// // userOne=1; //error

// let userTwo: {
//   name: string;
//   age: number;
//   male: boolean;
// };
// userTwo = { name: "bcd", age: 15, male: false };
// // userTwo={name:'bcd',age:15,male:false,skills:''}; //error

// // =================== Dynamic (any) Types =======================

// let age: any = 5;
// age = 26;
// age = "15";
// age = true;

// // ******* array ********
// let mixed: any[] = [];
// mixed.push("scrmt");
// mixed.push(5);
// mixed.push(false);

// // ******* objects ********

// let ninja: { name: any; age: any };
// ninja = { name: "kga", age: 15 };

// ninja = { name: 15, age: "kga" };

// // ninja={name:'kga',age:15,skills:[]}; //error
