type StringOrNum = string | number;

type objWithName = { name: string; uid: StringOrNum };

const logDetails = (uid: StringOrNum, item: string) => {
  console.log(`${item} has a uid of ${uid}`);
};
const greet = (user: objWithName) => {
  console.log(`${user.name} says hello`);
};
// const showInfoUser:Function=(userInfo:{name:string,age:number})=>{
//   console.log(userInfo);
// }
// const updateUser:Function=(id:number):{name:string,age:number}=>{
//   let newVersionUser:{name:string,age:number}={name:'abc',age:15};
//   ///update method....
//   return  newVersionUser;
// }
type objUser = { name: string; age: number };
const showInfoUser: Function = (userInfo: objUser) => {
  console.log(userInfo);
};
const updateUser: Function = (id: number): objUser => {
  let newVersionUser: objUser = { name: "abc", age: 15 };
  ///update method....
  return newVersionUser;
};
