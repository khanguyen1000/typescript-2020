// Generics
const addUID = (obj: object) => {
  let uid = Math.floor(Math.random() * 100);
  return { ...obj, uid };
};
let docOne = addUID({ name: "Kha", age: 21 });

// console.log(docOne.name); //error
/**
 *  do khi chung ta truyền đối tượng này vào hàm addUID
 *  nó sẽ không chỉ định chính xác đối tượng này có phải đúng nó hay là không
 *  nên thành ra nó sẽ lỗi ko tồn tại key đó
 *  cách fix sử dụng một chử cái để định danh cho nó
 *  vì khi dùng nó sẽ giúp chung ta năm bắt chính xác đối tượng được truyền vào
 */

const addUID1 = <X>(obj: X) => {
  let uid = Math.floor(Math.random() * 100);
  return { ...obj, uid };
};
let docOne1 = addUID1({ name: "Kha", age: 21 });
let docTwo1 = addUID1({ ten: "Jobn" });
console.log(docOne1.name, docTwo1.ten);
// sẽ có trường hợp cố tình ko nhập input vào là 1 object
let docThree = addUID1("sdasdas");

// ko gập lỗi do ta T chỉ là định danh để mún input luôn luôn là object
const addUID2 = <T extends object>(obj: T) => {
  let uid = Math.floor(Math.random() * 100);
  return { ...obj, uid };
};
let docOne2 = addUID2({ name: "Kha", age: 21 });
let docTwo2 = addUID2({ ten: "Jobn" });
console.log(docOne2.name, docTwo2.ten);
// let docThree2 = addUID2("asd"); //error
type objPerson ={name:string,age:number};
const addUID3 = <X extends objPerson>(obj: X) => {
  let uid = Math.floor(Math.random() * 100);
  return { ...obj, uid };
};
let docOne3 = addUID3({ name: "Kha", age: 21 });
let docTwo = addUID3({ name: "Kha", age: 21,skills:[] });
console.log(docOne3.name);

// ========== with InterFaces  ==========

interface Resource<T> {
  uid: number;
  resourceName: string;
  data: T;
}

const docThreeR: Resource<string> = {
  uid: 1,
  resourceName: "person",
  data: "shaul",
};
const docFourR: Resource<object> = {
  uid: 1,
  resourceName: "person",
  data: { name: "sa" },
};
const docFiveR: Resource<string[]> = {
  uid: 1,
  resourceName: "person",
  data: ["John", "Peter"],
};
console.log(docThreeR, docFourR, docFiveR);




