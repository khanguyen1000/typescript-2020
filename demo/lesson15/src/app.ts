import { Invoive } from "./classes/invoice.js";
import { ListTemplate } from "./classes/listTemplate.js";
import { Payment } from "./classes/payment.js";
import { HasFormatter } from "./interfaces/HasFormatter.js";

const form = document.querySelector(".new-item-form") as HTMLFormElement;
// inputs
const type = document.querySelector("#type") as HTMLSelectElement;
const toFrom = document.querySelector("#toFrom") as HTMLInputElement;
const details = document.querySelector("#details") as HTMLInputElement;
const amount = document.querySelector("#amount") as HTMLInputElement;

// list template instace
const ul = document.querySelector(".item-list") as HTMLUListElement;
const list = new ListTemplate(ul);

form.addEventListener("submit", (e: Event) => {
  e.preventDefault();

  let values: [string, string, number];
  values = [toFrom.value, details.value, amount.valueAsNumber];

  let doc: HasFormatter;
  if (type.value === "invoice") {
    doc = new Invoive(...values);
  } else {
    doc = new Payment(...values);
  }
  list.render(doc, type.value, "end");
});

// basic array
let arr = ["ryu", 25, true];

arr[0] = false;
arr[1] = "yosil";
arr = [30, false, "Yoals"];

// Tuples

let tup: [string, number, boolean] = ["ryu", 25, true];
tup[0] = "ken";

let student: [string, number];
student = ["chun-li", 2131];
