
// if(!localStorage.getItem("cuaHang.id")){
//     localStorage.setItem("cuaHang.id", i);
// }
let i = 2;
document.getElementById("btnThemDiaChiMoi").addEventListener('click', () => {
    let htmlLayout = `
    <div class="my-2">
        <h6>địa chỉ ${i}:</h6>
        <div class="row bg-light ">
            <div class="form-group col-3">
                <label for="soNha">số Nhà: </label>
                <input type="text" class="form-control soNha" required>
            </div>
            <div class="form-group col-3">
                <label for="tenDuong">tên đường: </label>
                <input type="text" class="form-control tenDuong" required>
            </div>
            <div class="form-group col-3">
                <label for="quanHuyen">Quận huyện: </label>
                <input type="text" class="form-control quanHuyen" required>
            </div>
            <div class="form-group col-3">
                <label for="thanhPho">Thành phố: </label>
                <input type="text" class="form-control thanhPho" required>
            </div>
        </div>
    </div>
`;

    document.getElementById("groupDiaChi").innerHTML += htmlLayout;
    i++;
})
let iUuDai = 2;
document.getElementById("btnThemUuDaiMoi").addEventListener('click', () => {
    let htmlLayout = `
    <div class="my-2">
        <h6>Ưu đãi ${iUuDai}:</h6>
        <div class="row bg-light ">
            <div class="form-group col-4">
                <div class="form-group">
                    <label for="">Loại ưu đãi</label>
                    <select class="form-control loaiUuDai" required>
                        <option value="1">Discount</option>
                        <option value="2">Freeship</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-8">
                <label for="tenDuong">nội dung ưu đãi: </label>
                <input type="text" class="form-control noiDungUuDai" required>
            </div>
        </div>
    </div>
`;

    document.getElementById("groupUuDai").innerHTML += htmlLayout;
    iUuDai++;
})



// event them mớn ăn 
let iMonAn = 2;

const handleAddMonAn = (btnClick, grouplayout, IThucDonMoi) => {
    console.log(btnClick, grouplayout);
    document.getElementById(btnClick)?.addEventListener('click', () => {
        let htmlLayout = `
    <div class="col-3 p-2">
        <div class="${iMonAn % 2 === 0 ? "bg-secondary" : "bg-dark"}  text-white p-2">
            <label>Món ${iMonAn}:</label>
            <div class="form-group">
                <label>Tên món: </label>
                <input type="text" class="form-control tenMon${IThucDonMoi}" required>
            </div>
            <div class="form-group">
                <label>Hình ảnh: </label>
                <input type="text" class="form-control hinhAnh${IThucDonMoi} " required>
            </div>
            <div class="form-group">
                <label>Giá món: </label>
                <input type="text" class="form-control giaMon${IThucDonMoi}" required>
            </div>
        </div>
        
    </div>

`
        document.getElementById(grouplayout).innerHTML += htmlLayout;
        iMonAn++;
    })
}
handleAddMonAn(`btnThemMonAnMoi1`, `groupMonAn1`);
let IThucDonMoi = 1;
document.getElementById("btnThemThucDonMoi")?.addEventListener("click", () => {
    iMonAn = 1;
    let htmlLayout = `
        <div class="my-2">
            <h6>Thực đơn ${IThucDonMoi}:</h6>
            <div class="row bg-light ">
                <div class="form-group col-12">
                    <label for="">Tên thực đơn:</label>
                    <input type="text" class="form-control tenThucDon" required>
                </div>
                <div class="d-flex align-items-center col-12">
                    <h5>Món ăn: </h5>
                    <button type="button" id="btnThemMonAnMoi${IThucDonMoi}" class="btn btn-success m-4">+</button>
                </div>
                <div class="row col-12" id="groupMonAn${IThucDonMoi}">
                    <div class="col-3 p-2 ">
                        <div class="bg-dark text-white p-2">
                            <label>Món 1:</label>
                            <div class="form-group">
                                <label>Tên món: </label>
                                <input type="text" class="form-control tenMon${IThucDonMoi}" required>
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh: </label>
                                <input type="text" class="form-control hinhAnh${IThucDonMoi}" required>
                            </div>
                            <div class="form-group">
                                <label>Giá món: </label>
                                <input type="text" class="form-control giaMon${IThucDonMoi}" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
    document.getElementById("groupThucDon").innerHTML += htmlLayout;
    handleAddMonAn(`btnThemMonAnMoi${IThucDonMoi}`, `groupMonAn${IThucDonMoi}`, IThucDonMoi);
    IThucDonMoi++;
})






// localStorage.setItem('lstCuaHang', []);
// let idCuaHang = localStorage.getItem("idCuaHang");

let idCuaHang = 1;

document.getElementById("FormSubmit").addEventListener('submit', (e) => {
    e.preventDefault();
    let tenCuaHang = document.getElementById("tenCuaHang").value;
    let diaChi = [];
    let soNhas = document.getElementsByClassName("soNha");
    let tenDuongs = document.getElementsByClassName("tenDuong");
    let quanHuyens = document.getElementsByClassName(" quanHuyen");
    let thanhPhos = document.getElementsByClassName(" thanhPho");
    let danhGia = parseFloat(document.getElementById("danhGia").value);
    let gioMoCua = document.getElementById('gioMoCua').value;
    let gioDongCua = document.getElementById('gioDongCua').value;
    let hinhAnhCuaHang = document.getElementById('hinhAnhCuaHang').value;
    let uuDai = [];
    let loaiUuDais = document.getElementsByClassName("loaiUuDai");
    let noiDungUuDais = document.getElementsByClassName("noiDungUuDai");
    let thucDon = [];
    for (let i = 0; i < soNhas.length; i++) {
        let diaChiItem = {
            id: i + 1,
            soNha: soNhas[i].value,
            tenDuong: tenDuongs[i].value,
            quanHuyen: quanHuyens[i].value,
            thanhPho: thanhPhos[i].value,
            diaChiFull: `${soNhas[i].value} ${tenDuongs[i].value}, ${quanHuyens[i].value}, ${thanhPhos[i].value}`
        }
        diaChi.push(diaChiItem);
    }
    for (let i = 0; i < loaiUuDais.length; i++) {
        let uuDaiItem = {
            id: i + 1,
            loaiUuDai: loaiUuDais[i].value,
            noiDungUuDai: noiDungUuDais[i].value
        }
        uuDai.push(uuDaiItem);
    }
    let lstThucDon = [];
    let tenThucDons = document.getElementsByClassName("tenThucDon");

    for (let i = 1; i <= tenThucDons.length; i++) {
        let lstMonAn = [];
        tenMons = document.getElementsByClassName(`tenMon${i}`);
        hinhAnhs = document.getElementsByClassName(`hinhAnh${i}`);
        giaMons = document.getElementsByClassName(`giaMon${i}`);
        for (let j = 0; j < tenMons.length; j++) {
            let monAnItem = {
                id: j + 1,
                tenMon: tenMons[j].value,
                hinhAnh: hinhAnhs[j].value,
                giaMon: giaMons[j].value
            }
            lstMonAn.push(monAnItem);
        }
        let ThucDonItem = {
            id: i,
            tenThucDon: tenThucDons[i - 1].value,
            lstMonAn
        }
        lstThucDon.push(ThucDonItem);
    }




    let CuaHang = {
        id: idCuaHang,
        tenCuaHang,
        diaChi,
        danhGia,
        gioMoCua,
        gioDongCua,
        hinhAnhCuaHang,
        uuDai,
        lstThucDon,
    }
    console.log(CuaHang);

    localStorage.setItem('lstCuaHang', [...CuaHang]);

});