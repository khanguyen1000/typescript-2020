"use strict";
// vì trong các ví dụ trong đây chúng ta sẽ phải sử dụng đế các kĩ thuật mới của ES6 nên chúng ta nâng cấp trình biên soạn này há.
// terminal => gõ lện tsc --init
// mở file tsconfig.json cấu hình version mới nhât
// tuples
//  tuples nó cũng giống như mảng mixed của mình ở bài trước
let arr = ['kha', 25, true];
arr[0] = false;
arr[1] = 'Cybersoft';
arr = [30, true, 'Cyberlearn'];
// nhưng tuples nó sẽ khác ở chỗ là .. mình có 1 cái biến
// let tup:[string,number,boolean] = ['Kha',25,false];
// nhưng nếu mình thây đổi vị trí cho nó thì sẽ báo lỗi
// let tup: [string, number, boolean] = [25,'Kha', false];
let tup = ['Kha', 25, false];
// tup[0] = 'as';
// tup[0] = 12;
// tup[1] = 12;
// tup[1] = 'as';
// mình có một mảng tên là student có phần tử thứ 1 là string, thứ 2 l2a number
let student;
// cái này thì mình sẽ hiểu như sau
student = ['kha nguyen', 21];
// đối tượng student có phần từ thứ nhất đại diện cho tên của nó thứ 2 đại diện cho tuổi của nó. thì bạn có thể dùng student kiểu object cũng có thể dùng theo kiểu tuple arr nhưng thế này cũng được
// ví dụ như bạn có 1 cai class student đi nhá
class studentClass {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
// khởi tạo class này há
// let st1 = new studentClass('kha',21);
// mình có 1 biến student v á thay gì mình gọi nếu data là 1 object thì nó sẽ nt nay
let obtStudent = {
    name: 'kha',
    age: 21,
};
let st1 = new studentClass(obtStudent.name, obtStudent.age);
// tuples sẽ xử lý như sau
let tupStudent = ['kha', 21];
// thì mình sẽ dùng RestParams há
let st2 = new studentClass(...tupStudent);
console.log(st1, st2);
// thì đó là cái ứng dụng của tuple há
// Enums
// là một kiểu dữ liệu đặc biệt của typescript há
var Color;
(function (Color) {
    Color[Color["Red"] = 1] = "Red";
    Color["Green"] = "G";
    Color[Color["Blue"] = 2] = "Blue";
    Color["Yellow"] = "Y";
    Color[Color["Black"] = 3] = "Black";
    Color["White"] = "W";
})(Color || (Color = {}));
let c = Color.Yellow;
console.log(c);
// nếu như mình không có quy định giá trị ban đầu cho nó. thì nó sẻ lấy vị trí của nó làm giá trị
// về ứng dụng của kiểu dữ liệu đặc biệt này !
let user;
//  1 Male   2 female
//  0 admin  1 highManager 2 manager 3 user
user = {
    username: 'admin',
    gender: 1,
    position: 0,
};
console.log(user);
// vì trong quá trình xây dựng bạn không thể nào nhớ nổi các quy định này.
// nên thành ra enum sẽ hiệu quả trong trường hợp này
var GenderEnum;
(function (GenderEnum) {
    GenderEnum[GenderEnum["Male"] = 1] = "Male";
    GenderEnum[GenderEnum["Female"] = 2] = "Female";
})(GenderEnum || (GenderEnum = {}));
var positionEnum;
(function (positionEnum) {
    positionEnum[positionEnum["admin"] = 0] = "admin";
    positionEnum[positionEnum["higManager"] = 1] = "higManager";
    positionEnum[positionEnum["manager"] = 2] = "manager";
    positionEnum[positionEnum["user"] = 3] = "user";
})(positionEnum || (positionEnum = {}));
let user1;
user = {
    username: 'kha',
    gender: GenderEnum.Male,
    position: positionEnum.admin,
};
console.log(user);
