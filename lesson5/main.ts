// Rồi xin chao các bạn nha thì video hôm nay chúng ta sẽ nói về functions há

let greet = () => {
  console.log('Hello world');
};

// greet = "hello";

let greet1: Function;
greet1 = () => {
  console.log('hello,again');
};
greet1 = (a: number) => {
  return a;
};
greet1();

const add = (a: number, b: number) => {
  console.log(a + b);
};

add(5, 10); //15
// add(5, '10');
// add(5);

const minus = (a: number, b: number) => {
  return a - b;
};

let result = minus(10, 7); //3

// result = "something else";

const minus1 = (a: number, b: number) => {
  return '';
};
let result1 = minus1(10, 7);

// mình không cần phải khai báo number vì typescript sẽ tự động kiểm tra kiểu dữ liệu trả về cho hàm đó và xét loại dữ liệu cho hàm đó.

// functions signatures thay vì viết thường như ở trên thì mình sẽ viết nâng cao lên há

const add1 = (a: number, b: number, c?: string | number) => {
  console.log(a + b);
  console.log(c);
};

add1(5, 10);
add1(5, 12, 21);
// default value
const add2 = (a: number, b: number, c: string | number = 10): void => {
  console.log(a + b);
  console.log(c);
};
add2(5, 10);
add2(5, 10, 11);
add2(5, 10, '11');

let add3: (a: number, b: number, c: string) => void;

add3 = (numOne: number, numTwo: number, action: string) => {
  return 12;
};
let a = add3(1, 5, '');
a = '';

a = 12;
// a=add1(1,2,5);

// spread Operator

let SpreadOperator: (...array: string[]) => void;

SpreadOperator = (...arr: Array<string>) => {
  console.log(arr);
};
