export class PhieuXuat {
    constructor(xuatDen, nameProduct, amount) {
        this.xuatDen = xuatDen;
        this.nameProduct = nameProduct;
        this.amount = amount;
    }
    CalcThanhTien() {
        return this.amount * 10000 - this.amount * 10000 * 0.1;
    }
    render(i) {
        const tr = document.createElement("tr");
        //#region cách 1
        // td
        /**
        const td_stt = document.createElement("td");
        const td_type = document.createElement("td");
        const td_Calc = document.createElement("td");
        const td_xuatDen = document.createElement("td");
        const td_nameProduct = document.createElement("td");
        const td_amount = document.createElement("td");
        td_stt.innerText = i + "";
        tr.append(td_stt);
        td_type.innerText = "Xuất kho";
        tr.append(td_type);
        td_xuatDen.innerText = this.xuatDen;
        tr.append(td_xuatDen);
        td_nameProduct.innerText = this.nameProduct;
        tr.append(td_nameProduct);
        td_amount.innerText = this.amount + "";
        tr.append(td_amount);
        td_Calc.innerText = this.CalcThanhTien() + "";
        tr.append(td_Calc);
        return tr;
         */
        //#endregion
        //#region cách 2 sử dụng string template
        let trTemplate = `<td scope="row">${i}</td>
    <td>Xuất kho</td>
    <td>${this.xuatDen}</td>
    <td>${this.nameProduct}</td>
    <td>${this.amount}</td>
    <td>${this.CalcThanhTien()}</td>`;
        tr.innerHTML = trTemplate;
        return tr;
        //#endregion
    }
}
// import { IPhieu } from "../interfaces/IPhieu.js";
// export class PhieuXuat implements IPhieu {
//   xuatDen: string; //thuộc tính riêng cảu class Phieu xu
//   nameProduct: string;
//   amount: number;
//   constructor(xuatDen: string, nameProduct: string, amount: number) {
//     this.xuatDen = xuatDen;
//     this.nameProduct = nameProduct;
//     this.amount = amount;
//   }
//   CalcThanhTien(): number {
//     return this.amount * 10000 - this.amount * 10000 * 0.1;
//   }
// }
