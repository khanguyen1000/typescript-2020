export class PhieuNhap {
    constructor(nhapTu, nameProduct, amount) {
        this.nhapTu = nhapTu;
        this.nameProduct = nameProduct;
        this.amount = amount;
    }
    CalcThanhTien() {
        return this.amount * 10000;
    }
    render(i) {
        const tr = document.createElement("tr");
        //#region cách 1
        /**
          const td_stt = document.createElement("td");
        const td_type = document.createElement("td");
        const td_Calc = document.createElement("td");
        const td_nhapTu = document.createElement("td");
        const td_nameProduct = document.createElement("td");
        const td_amount = document.createElement("td");
        td_stt.innerText = i + "";
        tr.append(td_stt);
        td_type.innerText = "Nhập kho";
        tr.append(td_type);
        td_nhapTu.innerText = this.nhapTu;
        tr.append(td_nhapTu);
        td_nameProduct.innerText = this.nameProduct;
        tr.append(td_nameProduct);
        td_amount.innerText = this.amount + "";
        tr.append(td_amount);
        td_Calc.innerText = this.CalcThanhTien() + "";
        tr.append(td_Calc);
        return tr;
         */
        //#endregion
        //#region cách 2 sử dụng string template
        let trTemplate = `<td scope="row">${i}</td>
    <td>Nhập kho</td>
    <td>${this.nhapTu}</td>
    <td>${this.nameProduct}</td>
    <td>${this.amount}</td>
    <td>${this.CalcThanhTien()}</td>`;
        tr.innerHTML = trTemplate;
        return tr;
        //#endregion
    }
}
// import { IPhieu } from "../interfaces/IPhieu.js";
// export class PhieuNhap implements IPhieu {
//   nhapTu: string;
//   nameProduct: string;
//   amount: number;
//   constructor(nhapTu: string, nameProduct: string, amount: number) {
//     this.nhapTu = nhapTu;
//     this.nameProduct = nameProduct;
//     this.amount = amount;
//   }
//   CalcThanhTien(): number {
//     return this.amount * 10000;
//   }
// }
