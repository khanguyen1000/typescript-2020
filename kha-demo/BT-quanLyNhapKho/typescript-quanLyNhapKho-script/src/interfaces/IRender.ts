export interface IRender {
  render(i: number): HTMLElement;
}
