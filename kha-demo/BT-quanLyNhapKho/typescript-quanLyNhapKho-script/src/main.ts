import {IPhieu} from './interfaces/IPhieu.js';
import {PhieuNhap} from './classes/PNhap.js';
import {PhieuXuat} from './classes/PXuat.js';
import {IRender} from './interfaces/IRender.js';
// Form
const form = document.querySelector('.new-item-form') as HTMLFormElement;
// inputs
const type = document.querySelector('#type') as HTMLSelectElement;
const tofrom = document.querySelector('#tofrom') as HTMLInputElement;
const productName = document.querySelector('#productName') as HTMLInputElement;
const amount = document.querySelector('#amount') as HTMLInputElement;
const tbody = document.querySelector('#renderList') as HTMLTableDataCellElement;
const xoaTrang: Function = () => {
  type.value = 'import';
  tofrom.value = '';
  productName.value = '';
  amount.value = '';
};
// tbody.innerHTML = `<tr>
// <td scope="row">1</td>
// <td>Nhập kho</td>
// <td>Mexio</td>
// <td>Máy Lạnh</td>
// <td>16</td>
// <td>250000</td>
// </tr>`;
/**
    
 */
// events submit
// form.addEventListener("submit", (e: Event) => {
//   e.preventDefault();
//   let phieu;
//   if (type.value === "export") {
//     phieu = new PhieuXuat(
//       tofrom.value,
//       productName.value,
//       amount.valueAsNumber
//     );
//   } else {
//     phieu = new PhieuNhap(
//       tofrom.value,
//       productName.value,
//       amount.valueAsNumber
//     );
//   }
//   console.log(phieu);
// });
// events submit with interfaces
let i = 1;
form.addEventListener('submit', (e: Event) => {
  e.preventDefault();
  let phieu: IPhieu & IRender;
  if (type.value === 'export') {
    phieu = new PhieuXuat(
      tofrom.value,
      productName.value,
      amount.valueAsNumber
    );
  } else {
    phieu = new PhieuNhap(
      tofrom.value,
      productName.value,
      amount.valueAsNumber
    );
  }
  let tr = phieu.render(i); // phieu.render() trả một thẻ tr
  tbody.append(tr); // nạp chồng thẻ đó vào tbody bằng hàm append
  i++; // trước khi kết thúc sự kiện thì tăng i lên một đơn vị.
  xoaTrang();
});
