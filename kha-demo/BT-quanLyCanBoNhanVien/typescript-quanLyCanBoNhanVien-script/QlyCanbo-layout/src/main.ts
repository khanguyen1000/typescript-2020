import { CompanyServices } from "./classes/CompanyServices.js";
import { Empl_IT } from "./classes/Empl_IT.js";
import { Empl_Mark } from "./classes/Empl_Mark.js";
import { Empl_Sal } from "./classes/Empl_Sal.js";
import { IEmpl } from "./interfaces/IEmpl.js";
declare let swal: any;
declare let $: any;
let roleModal = -1; //role =1 thêm // role =2 cập nhật
type listEmployeesType = (Empl_Mark | Empl_Sal | Empl_IT)[];
// DOM input
let txtName = document.querySelector("#txtName") as HTMLInputElement;
let txtAge = document.querySelector("#txtAge") as HTMLInputElement;
let txtGender = document.querySelector("#txtGender") as HTMLSelectElement;
let txtTimeWorked = document.querySelector(
  "#txtTimeWorked"
) as HTMLInputElement;
let radIT = document.querySelector("#radIT") as HTMLInputElement;
let radMark = document.querySelector("#radMark") as HTMLInputElement;
let radSal = document.querySelector("#radSal") as HTMLInputElement;

let listEmployee: listEmployeesType = [];

const ctyServices = new CompanyServices();

// mapdata Function
const RenderLayout: Function = (): void => {
  const tbody = document.querySelector(
    "#tb_render"
  ) as HTMLTableDataCellElement;
  let layoutHTML: string = "";
  let i = 1;
  for (let item of listEmployee) {
    layoutHTML += item.renderLayout(i);
    i++;
  }
  tbody.innerHTML = layoutHTML;

  for (let item of listEmployee) {
    UpdateRowMethod(item.id);
    deleteRowMethod(item.id);
  }
};
const mapData: Function = (data: any): void => {
  listEmployee = data.map((item: any) => {
    let Empl: IEmpl;
    if (item.department === "it")
      return (Empl = new Empl_IT(
        item.id,
        item.name,
        item.age,
        item.gender,
        item.timeWorked,
        item.department
      ));
    if (item.department === "marketing")
      return (Empl = new Empl_Mark(
        item.id,
        item.name,
        item.age,
        item.gender,
        item.timeWorked,
        item.department
      ));
    else item.department === "sales";
    return (Empl = new Empl_Sal(
      item.id,
      item.name,
      item.age,
      item.gender,
      item.timeWorked,
      item.department
    ));
  });
  RenderLayout();
};

const callModal: Function = (modal_title: string, type = 1): void => {
  // type ===1 là thêm một nhân viên mới
  // type ===2 là cập nhật một nhân viên
  let titleTag = document.querySelector(
    "#modalAddNewLabel"
  ) as HTMLHeadingElement;
  titleTag.innerText = modal_title;
  let btnSubmit = document.querySelector("#btnSubmit") as HTMLButtonElement;

  switch (type) {
    case 1: {
      roleModal = 1;
      btnSubmit.innerText = "Thêm";
      break;
    }
    case 2: {
      roleModal = 2;
      btnSubmit.innerText = "Cập nhật";
      break;
    }
  }
  $("#myModal").modal({
    show: true,
  });
};

ctyServices.getListEmpl().then((res: any) => {
  mapData(res.data);
});

let idUpdate: string = "";
const UpdateRowMethod: Function = (id: string): void => {
  let btn = document.querySelector(`#btnUpdate_${id}`) as HTMLButtonElement;
  btn.addEventListener("click", () => {
    callModal("Cập nhật", 2);
    $("#modalAddNew").modal({
      show: true,
    });
    idUpdate = id;
    ctyServices.getEmplByID(id).then((res: any) => {
      console.log(res.data);
      const { name, age, gender, timeWorked, department } = res.data;
      txtName.value = name;
      txtAge.valueAsNumber = age;
      txtGender.value = gender;
      txtTimeWorked.value = timeWorked;
      if (department === "marketing") radMark.checked = true;
      else if (department === "it") radIT.checked = true;
      else radSal.checked = true;
    });
  });
};
const deleteRowMethod: Function = (id: string): void => {
  let btn = document.querySelector(`#btnDelete_${id}`) as HTMLButtonElement;
  btn.addEventListener("click", () => {
    swal({
      title: "Bạn chắc chứ?",
      text: "nếu đồng ý, tất cả dữ liệu của employee này sẽ bị mất hoàn toàn",
      icon: "info",
      buttons: true,
    }).then((willDelete: boolean) => {
      if (willDelete) {
        ctyServices.deleteEmpl(id).then((res: any) => {
          swal("dữ liệu đã được xóa hoàn toàn !", {
            icon: "success",
          });
        });
      } else {
        swal("mọi thứ an toàn!");
      }
    });
  });
};

document.querySelector("#btnShowModalAddNew")?.addEventListener("click", () => {
  callModal("Thêm Mới", 1);
  $("#modalAddNew").modal({
    show: true,
  });
});

const RemoveText: Function = (): void => {
  txtName.value = "";
  txtAge.value = "0";
  txtGender.value = "male";
  txtTimeWorked.value = "0";
  radIT.checked = true;
};
let form = document.querySelector("#frm-add") as HTMLFormElement;
form.addEventListener("submit", (e: Event) => {
  e.preventDefault();
  let depart = radIT.checked
    ? radIT.value
    : radMark.checked
    ? radMark.value
    : radSal.value;

  let data = {
    name: txtName.value,
    age: txtAge.valueAsNumber,
    gender: txtGender.value,
    timeWorked: txtTimeWorked.valueAsNumber,
    department: depart,
  };
  console.log(roleModal);
  if (roleModal === 1) {
    ctyServices.addNewEmpl(data).then((res: any) => {
      swal({
        title: `Thêm thành công`,
        icon: "success",
      });
      console.log(res.data);
      RemoveText();
    });
  } else {
    ctyServices.updateEmpl(idUpdate, data).then((res: any) => {
      swal({
        title: `Cập nhật thành công`,
        icon: "success",
      });
      console.log(res.data);
      RemoveText();
    });
  }
});

// onchange select search deparment
let selSearchDepartment = document.querySelector(
  "#selSearchDeparment"
) as HTMLInputElement;
selSearchDepartment.addEventListener("change", (e: Event) => {
  let target: any = e.target;
  console.log(target.value);
  if (target.value === "all") {
    ctyServices.getListEmpl().then((res: any) => mapData(res.data));
  } else {
    ctyServices
      .findEmplWithDepartment(target.value)
      .then((res: any) => mapData(res.data));
  }
});
