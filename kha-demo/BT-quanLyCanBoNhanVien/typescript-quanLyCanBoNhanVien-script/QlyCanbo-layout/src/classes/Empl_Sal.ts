import { IEmpl } from "../interfaces/IEmpl.js";

export class Empl_Sal implements IEmpl {
  id: string;
  name: string;
  age: number;
  gender: string;
  timeWorked: number;
  department: string;
  constructor(
    id: string,
    name: string,
    age: number,
    gender: string,
    timeWorked: number,
    department: string
  ) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.gender = gender;
    this.timeWorked = timeWorked;
    this.department = department;
  }
  calcSalary = (): number => this.timeWorked * 48000 * 1.5;
  renderLayout(stt: number): string {
    let layout: string = "";
    layout = ` 
    <tr>
        <td scope="row">${stt}</td>
        <td>${this.name}</td>
        <td>${this.age}</td>
        <td>${this.gender}</td>
        <td>${this.department}</td>
        <td>${this.calcSalary().toLocaleString()}</td>
        <td>
            <button class="btn btn-primary" id="btnUpdate_${
              this.id
            }"><i class="fas fa-pencil-alt"></i></button>
            <button class="btn btn-primary" id="btnDelete_${
              this.id
            }"><i class="far fa-window-close"></i></button>
        </td>
    </tr>   
`;

    return layout;
  }
}
