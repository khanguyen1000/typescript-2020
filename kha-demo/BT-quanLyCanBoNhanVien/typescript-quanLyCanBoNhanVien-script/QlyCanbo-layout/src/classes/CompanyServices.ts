declare let axios: any;

export class CompanyServices {
  constructor() {}
  // get list employees
  getListEmpl(): any {
    return axios({
      method: "get",
      url: "http://localhost:3000/employees",
    }).catch((err: any) => console.log(err));
  }
  // get employee by id
  getEmplByID(id: string): any {
    return axios({
      method: "get",
      url: `http://localhost:3000/employees/${id}`,
    }).catch((err: any) => console.log(err));
  }
  //   add new employee method
  addNewEmpl(empl: any): any {
    return axios({
      method: "post",
      url: "http://localhost:3000/employees",
      data: empl,
    }).catch((err: any) => console.log(err));
  }
  deleteEmpl(id: string): any {
    return axios({
      method: "delete",
      url: `http://localhost:3000/employees/${id}`,
    }).catch((err: any) => console.log(err));
  }
  updateEmpl(id: string, empl: any): any {
    return axios({
      method: "patch",
      url: `http://localhost:3000/employees/${id}`,
      data: empl,
    }).catch((err: any) => console.log(err));
  }
  findEmplWithDepartment(depart: string): any {
    return axios({
      method: "get",
      url: `http://localhost:3000/employees/?department=${depart}`,
    }).catch((err: any) => console.log(err));
  }
}
