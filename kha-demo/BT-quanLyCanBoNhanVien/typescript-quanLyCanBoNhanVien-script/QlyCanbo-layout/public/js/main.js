var _a;
import { CompanyServices } from "./classes/CompanyServices.js";
import { Empl_IT } from "./classes/Empl_IT.js";
import { Empl_Mark } from "./classes/Empl_Mark.js";
import { Empl_Sal } from "./classes/Empl_Sal.js";
let roleModal = -1; //role =1 thêm // role =2 cập nhật
// DOM input
let txtName = document.querySelector("#txtName");
let txtAge = document.querySelector("#txtAge");
let txtGender = document.querySelector("#txtGender");
let txtTimeWorked = document.querySelector("#txtTimeWorked");
let radIT = document.querySelector("#radIT");
let radMark = document.querySelector("#radMark");
let radSal = document.querySelector("#radSal");
let listEmployee = [];
const ctyServices = new CompanyServices();
// mapdata Function
const RenderLayout = () => {
    const tbody = document.querySelector("#tb_render");
    let layoutHTML = "";
    let i = 1;
    for (let item of listEmployee) {
        layoutHTML += item.renderLayout(i);
        i++;
    }
    tbody.innerHTML = layoutHTML;
    for (let item of listEmployee) {
        UpdateRowMethod(item.id);
        deleteRowMethod(item.id);
    }
};
const mapData = (data) => {
    listEmployee = data.map((item) => {
        let Empl;
        if (item.department === "it")
            return (Empl = new Empl_IT(item.id, item.name, item.age, item.gender, item.timeWorked, item.department));
        if (item.department === "marketing")
            return (Empl = new Empl_Mark(item.id, item.name, item.age, item.gender, item.timeWorked, item.department));
        else
            item.department === "sales";
        return (Empl = new Empl_Sal(item.id, item.name, item.age, item.gender, item.timeWorked, item.department));
    });
    RenderLayout();
};
const callModal = (modal_title, type = 1) => {
    // type ===1 là thêm một nhân viên mới
    // type ===2 là cập nhật một nhân viên
    let titleTag = document.querySelector("#modalAddNewLabel");
    titleTag.innerText = modal_title;
    let btnSubmit = document.querySelector("#btnSubmit");
    switch (type) {
        case 1: {
            roleModal = 1;
            btnSubmit.innerText = "Thêm";
            break;
        }
        case 2: {
            roleModal = 2;
            btnSubmit.innerText = "Cập nhật";
            break;
        }
    }
    $("#myModal").modal({
        show: true,
    });
};
ctyServices.getListEmpl().then((res) => {
    mapData(res.data);
});
let idUpdate = "";
const UpdateRowMethod = (id) => {
    let btn = document.querySelector(`#btnUpdate_${id}`);
    btn.addEventListener("click", () => {
        callModal("Cập nhật", 2);
        $("#modalAddNew").modal({
            show: true,
        });
        idUpdate = id;
        ctyServices.getEmplByID(id).then((res) => {
            console.log(res.data);
            const { name, age, gender, timeWorked, department } = res.data;
            txtName.value = name;
            txtAge.valueAsNumber = age;
            txtGender.value = gender;
            txtTimeWorked.value = timeWorked;
            if (department === "marketing")
                radMark.checked = true;
            else if (department === "it")
                radIT.checked = true;
            else
                radSal.checked = true;
        });
    });
};
const deleteRowMethod = (id) => {
    let btn = document.querySelector(`#btnDelete_${id}`);
    btn.addEventListener("click", () => {
        swal({
            title: "Bạn chắc chứ?",
            text: "nếu đồng ý, tất cả dữ liệu của employee này sẽ bị mất hoàn toàn",
            icon: "info",
            buttons: true,
        }).then((willDelete) => {
            if (willDelete) {
                ctyServices.deleteEmpl(id).then((res) => {
                    swal("dữ liệu đã được xóa hoàn toàn !", {
                        icon: "success",
                    });
                });
            }
            else {
                swal("mọi thứ an toàn!");
            }
        });
    });
};
(_a = document.querySelector("#btnShowModalAddNew")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", () => {
    callModal("Thêm Mới", 1);
    $("#modalAddNew").modal({
        show: true,
    });
});
const RemoveText = () => {
    txtName.value = "";
    txtAge.value = "0";
    txtGender.value = "male";
    txtTimeWorked.value = "0";
    radIT.checked = true;
};
let form = document.querySelector("#frm-add");
form.addEventListener("submit", (e) => {
    e.preventDefault();
    let depart = radIT.checked
        ? radIT.value
        : radMark.checked
            ? radMark.value
            : radSal.value;
    let data = {
        name: txtName.value,
        age: txtAge.valueAsNumber,
        gender: txtGender.value,
        timeWorked: txtTimeWorked.valueAsNumber,
        department: depart,
    };
    console.log(roleModal);
    if (roleModal === 1) {
        ctyServices.addNewEmpl(data).then((res) => {
            swal({
                title: `Thêm thành công`,
                icon: "success",
            });
            console.log(res.data);
            RemoveText();
        });
    }
    else {
        ctyServices.updateEmpl(idUpdate, data).then((res) => {
            swal({
                title: `Cập nhật thành công`,
                icon: "success",
            });
            console.log(res.data);
            RemoveText();
        });
    }
});
// onchange select search deparment
let selSearchDepartment = document.querySelector("#selSearchDeparment");
selSearchDepartment.addEventListener("change", (e) => {
    let target = e.target;
    console.log(target.value);
    if (target.value === "all") {
        ctyServices.getListEmpl().then((res) => mapData(res.data));
    }
    else {
        ctyServices
            .findEmplWithDepartment(target.value)
            .then((res) => mapData(res.data));
    }
});
