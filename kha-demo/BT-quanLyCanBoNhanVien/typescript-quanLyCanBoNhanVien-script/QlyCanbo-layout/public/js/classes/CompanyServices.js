export class CompanyServices {
    constructor() { }
    // get list employees
    getListEmpl() {
        return axios({
            method: "get",
            url: "http://localhost:3000/employees",
        }).catch((err) => console.log(err));
    }
    // get employee by id
    getEmplByID(id) {
        return axios({
            method: "get",
            url: `http://localhost:3000/employees/${id}`,
        }).catch((err) => console.log(err));
    }
    //   add new employee method
    addNewEmpl(empl) {
        return axios({
            method: "post",
            url: "http://localhost:3000/employees",
            data: empl,
        }).catch((err) => console.log(err));
    }
    deleteEmpl(id) {
        return axios({
            method: "delete",
            url: `http://localhost:3000/employees/${id}`,
        }).catch((err) => console.log(err));
    }
    updateEmpl(id, empl) {
        return axios({
            method: "patch",
            url: `http://localhost:3000/employees/${id}`,
            data: empl,
        }).catch((err) => console.log(err));
    }
    findEmplWithDepartment(depart) {
        return axios({
            method: "get",
            url: `http://localhost:3000/employees/?department=${depart}`,
        }).catch((err) => console.log(err));
    }
}
