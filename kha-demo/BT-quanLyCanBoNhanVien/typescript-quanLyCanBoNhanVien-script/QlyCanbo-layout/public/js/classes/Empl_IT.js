export class Empl_IT {
    constructor(id, name, age, gender, timeWorked, department) {
        this.calcSalary = () => this.timeWorked * 50000 * 1.3;
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.timeWorked = timeWorked;
        this.department = department;
    }
    renderLayout(stt) {
        let layout = "";
        layout = ` 
    <tr>
        <td scope="row">${stt}</td>
        <td>${this.name}</td>
        <td>${this.age}</td>
        <td>${this.gender}</td>
        <td>${this.department}</td>
        <td>${this.calcSalary().toLocaleString()}</td>
        <td>
            <button class="btn btn-primary"  id="btnUpdate_${this.id}"><i class="fas fa-pencil-alt"></i></button>
            <button class="btn btn-primary" id="btnDelete_${this.id}"><i class="far fa-window-close"></i></button>
        </td>
    </tr>   
`;
        return layout;
    }
}
