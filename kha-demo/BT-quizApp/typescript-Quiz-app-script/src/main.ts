declare let axios: any;
declare let swal: any;
import {IQues} from './intefaces/IQues.js';
import {answersChoise} from './types/answersChoiseType.js';
import {FillBlank} from './classes/FillBlank.js';
import {MulChoice} from './classes/mulChoice.js';
type typeList = (FillBlank | MulChoice)[];
let listQuestion: typeList = [];

fetch('../../listCauHoi.json')
  .then((res) => res.json()) // 'then' này là dùng để convert json qua object
  .then((data: any) => {
    // 'then' này trả về data đã dc convert
    // console.log(data);
    mapdata(data);
    // console.log(listQuestion);
    renderQuestion(); //gọi hàm renderQuestion ra thực thi
  })
  .catch((err: any) => console.log(err));
const mapdata: Function = (data: typeList): void => {
  listQuestion = data.map((item) => {
    //map mảng input và sau đó gáng mảng listQuestion
    let quesItem: IQues<answersChoise[] | string>;
    if (item.type === 'mulChoice') {
      return (quesItem = new MulChoice(
        item.id,
        item.type,
        item.ques,
        item.answers as answersChoise[] //gắn kiểu dữ liệu cho answers là answersChoise[]
      ));
    } else {
      return (quesItem = new FillBlank(
        item.id,
        item.type,
        item.ques,
        item.answers as string //gắn kiểu dữ liệu cho answers là string
      ));
    }
  });
};

// render function
const renderQuestion: Function = (): void => {
  let templa: string = '';
  // console.log(listQuestion); //list câu hỏi sau khi map
  for (let item of listQuestion) {
    templa += item.render(); // gọi phương thức render của từ class và cộng dồn vào templa
  }
  let Box_CauHoi = document.querySelector('#Box_CauHoi') as HTMLElement; //DOM đến Box câu hỏi
  Box_CauHoi.innerHTML = templa; //gán HTMl vào
  let total_Q = document.querySelector('#total_Q') as HTMLSpanElement; //DOM đến thẻ span số lượng câu hỏi
  total_Q.innerText = listQuestion.length + ''; //gán số lượng câu hỏi
};

// submit function

const handleCheckAnswers: Function = () => (e: Event) => {
  e.preventDefault();
  let res = 0; //biến res dể xuất ra số câu trả lời dúng
  for (let item of listQuestion) {
    if (item.checkAnswer()) {
      res++; // nếu kiểm tra true thì tăng biến res lên 1 đơn vị
    }
  }
  //DOm và xuất kết quả lên thẻ results
  let results = document.querySelector('#results') as HTMLSpanElement;
  results.innerText = res + '';
  // alert(`Result : ${res}`); //thông báo kết quả.
  swal({
    title: 'Good job!',
    text: `Result : ${res}`,
    icon: 'success',
  });
};
let form = document.querySelector('#form-quiz') as HTMLFormElement; //DOM dến form
// form.addEventListener('submit', handleCheckAnswers()); // khởi tạo sự kiện submit và gán hàm submit vào sự kiện
form.addEventListener('submit', (e: Event) => {
  e.preventDefault();
  let res = 0; //biến res dể xuất ra số câu trả lời dúng
  for (let item of listQuestion) {
    if (item.checkAnswer()) {
      res++; // nếu kiểm tra true thì tăng biến res lên 1 đơn vị
    }
  }
  //DOm và xuất kết quả lên thẻ results
  let results = document.querySelector('#results') as HTMLSpanElement;
  results.innerText = res + '';
  swal({
    title: 'Good job!',
    text: `Result : ${res}`,
    icon: 'success',
  }).then((oke: unknown) => {
    if (typeof oke === 'boolean') {
      if (oke) {
        form.reset();
        // console.log(form.checkValidity());
        let dome = document.getElementById('tag_1') as HTMLElement;
        dome.scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'nearest',
        });
      }
    }
  });
});
