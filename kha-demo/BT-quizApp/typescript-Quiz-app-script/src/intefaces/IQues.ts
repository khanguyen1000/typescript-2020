export interface IQues<T> {
  id: number;
  ques: string;
  answers: T;
  type: string;
  checkAnswer(): boolean;
  render(): string;
}
