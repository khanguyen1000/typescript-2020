import {IQues} from '../intefaces/IQues.js';
import {answersChoise} from '../types/answersChoiseType.js';
export class MulChoice implements IQues<answersChoise[]> {
  id: number;
  ques: string;
  type: string;
  answers: answersChoise[];
  constructor(
    id: number,
    type: string,
    ques: string,
    answers: answersChoise[]
  ) {
    this.id = id;
    this.ques = ques;
    this.type = type;
    this.answers = answers;
  }

  render(): string {
    let answTemplate = ``;
    for (let aws of this.answers) {
      answTemplate += `
      <div class=" answer_item"><input class="cau-${this.id}" type="radio" name="cau-mul-${this.id}" value="${aws.id}" required>
                      <span>${aws.title}</span>
      </div>
      `;
    }
    let template = `
    <div class="quest-item" id="tag_${this.id}">
      <div class="form-group">
          <div class="text-quest">Câu ${this.id}: ${this.ques}</div>
          <div class="container">
              <div class=" group-anwsers">
                  ${answTemplate}
              </div>
          </div>
      </div> 
  </div>
  `;
    return template;
  }
  checkAnswer(): boolean {
    let id: number = -1; //biến id dùng để lưu id của input nào đã checked
    const ipList = document.querySelectorAll(`.cau-${this.id}`); //DOM đến list ip có class l2a cau-#
    for (let item of ipList) {
      // duyệt lần lượt qua list input có class là cau-#
      let ip = item as HTMLInputElement; //gán lại kiểu cho từng thẻ
      if (ip.checked) {
        id = ip.valueAsNumber; // gán dữ liệu lại cho biến id
        break; // nếu đã tìm thấy được một thẻ check thì break khỏi vòng lập for
      }
    }
    if (id === -1) return false; // nếu id=-1 tức là không tìm check thì trả về false
    for (let item of this.answers) {
      // duyệt qua mảng answers
      if (item.id === id) {
        //kiểm tra dối tượng nào == biến id
        return item.exact; // trả về kết quả của đối tượng đó
      }
    }
    return false;
  }
}
