import {IQues} from '../intefaces/IQues.js';

export class FillBlank implements IQues<string> {
  id: number;
  ques: string;
  answers: string;
  type: string;
  constructor(id: number, type: string, ques: string, answers: string) {
    this.id = id;
    this.ques = ques;
    this.type = type;
    this.answers = answers;
  }

  render(): string {
    let template = `
    <div class="quest-item"  id="tag_${this.id}">
      <div class="form-group">
          <div class="text-quest">Câu ${this.id}: ${this.ques}</div>
          <div class="container">
              <input type="text" class="form-control cau-${this.id}" name="cau-fill-${this.id}" placeholder="${this.answers}" required>
          </div>
      </div>
    </div>
    `;
    return template;
  }
  checkAnswer(): boolean {
    let ip = document.querySelector(`.cau-${this.id}`) as HTMLInputElement; // DOM đến thẻ input
    if (ip.value.toLowerCase() === this.answers.toLowerCase()) {
      //kiểm tra xem text input có khớp với answers của mình không
      return true; //trả về true nếu đúng
    }
    return false; //trả về false nếu sai
  }
}
