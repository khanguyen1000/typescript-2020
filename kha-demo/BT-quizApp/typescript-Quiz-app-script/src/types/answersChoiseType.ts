import { MulChoice } from "../classes/mulChoice";

export type answersChoise = {
  id: number;
  title: string;
  exact: boolean;
};
