import { FillBlank } from "./FillBlank.js";
import { MulChoice } from "./mulChoice.js";
export class ListQuestion {
    constructor(listQuestion) {
        this.listQuestion = listQuestion;
    }
    render() {
        let templa = "";
        for (let ques of this.listQuestion) {
            let quesItem;
            if (ques.type === "mulChoice") {
                quesItem = new MulChoice(ques.id, ques.ques, ques.answers);
            }
            else {
                quesItem = new FillBlank(ques.id, ques.ques, ques.answers);
            }
            templa += quesItem.render();
        }
        return templa;
    }
    checkAnswers(listAnswers) {
        let result = 0;
        for (let item of this.listQuestion) {
            item;
        }
        return result;
    }
}
