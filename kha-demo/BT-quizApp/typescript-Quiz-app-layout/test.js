// const getlowercasedString = (myString) => {
//     return myString.toLowerCase();
// }
// getlowercasedString(undefined);




const getProductService = (response) => {
    if (response.data.hasSucceeded) {
        return `product name: ${response.name} `;
    } else {
        return 'API no response';
    }
}
const apiResponse = {
    data: {
        hasSucceeded: true,
        name: 'product 1',
    },
};
console.log(getProductService(apiResponse));